package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.starter.router.MainRouter;

public class MainVerticle  extends AbstractVerticle {

    public void start(){
        Router router = new MainRouter().makeRouter(vertx);
        vertx.createHttpServer()
                .requestHandler(router)
                .listen(8080);
    }
 }
