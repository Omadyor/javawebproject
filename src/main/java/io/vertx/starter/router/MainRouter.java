package io.vertx.starter.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;

import io.vertx.starter.controller.ChinaController;
import io.vertx.starter.controller.EnglandController;
import io.vertx.starter.controller.FranceController;
import io.vertx.starter.controller.GermanyController;
import io.vertx.starter.controller.JapanController;
import io.vertx.starter.controller.RussiaController;
import io.vertx.starter.controller.UzbekController;


public class MainRouter {

    private ChinaController China = new ChinaController();
    private EnglandController England = new EnglandController();
    private FranceController France = new FranceController();
    private GermanyController Germany = new GermanyController();
    private JapanController Japan = new JapanController();
    private RussiaController Russia = new RussiaController();
    private UzbekController Uzbek = new UzbekController();


    public Router makeRouter(Vertx vertx){

        Router router = Router.router(vertx);

        router.route("/china/area").handler(China::area);
        router.route("/england/area").handler(England::area);
        router.route("/france/area").handler(France::area);
        router.route("/germany/area").handler(Germany::area);
        router.route("/japan/area").handler(Japan::area);
        router.route("/russia/area").handler(Russia::area);
        router.route("/uzbek/area").handler(Uzbek::area);

        router.route("/china/population").handler(China::population);
        router.route("/england/population").handler(England::population);
        router.route("/france/population").handler(France::population);
        router.route("/germany/population").handler(Germany::population);
        router.route("/japan/population").handler(Japan::population);
        router.route("/russia/population").handler(Russia::population);
        router.route("/uzbek/population").handler(Uzbek::population);



        return router;

    }
}
