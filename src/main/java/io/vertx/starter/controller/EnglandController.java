package io.vertx.starter.controller;

import io.vertx.ext.web.RoutingContext;

public class EnglandController {
    public void area(RoutingContext context){
        String n="130,395 km.kv\n";
        context.response().end(n);
    }
    public void population(RoutingContext context){
        String n="54,79 million\n";
        context.response().end(n);
    }
}
